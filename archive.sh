#!/bin/bash

CURRENT_DIR=`pwd`
function Archive {
cd $SOURCE_DIR
for file in `ls`
do
	if [ -f $file ]
	then
		tar -rf $DESTINATION_DIR/archive_`date +%F--%H:%M`.tar $file
	fi
done
cd $CURRENT_DIR
}

Archive
