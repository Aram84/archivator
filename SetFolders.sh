#!/bin/bash

TMP=`readlink -f $0`
SCRIPT_SDIR=`dirname $TMP`

#Print help information about the script
function print_help {
echo -e "\n\033[31mUsage:\033[0m ./SetFolders.sh -t 'm h D M d'"
echo "Sets the source and destination folders for archivator and set time for crone.The argument -t is required"
echo -e "\033[32mOPTIONS\033[0m"
echo -e "\t-t	time for start archiver"
echo -e "\t--help	show help\n"


echo -e "\033[32mARGUMENTS FOR OPTION -t\033[0m"
echo "	m	minutes (0-59 or *)"
echo "	h	hours 	(0-23 or *)"
echo "	d	days of month 	(1-31 or *)"
echo "	M	months	(1-12 or *)"
echo "	d	days of week (0-6 or *)"
}

if [ $# -gt 2 ]
then 
	echo "Try './SetFolders.sh --help' for more information."
	exit 1
elif [ $1 == "--help" ]
then
	print_help
	exit 0
else
	while getopts "t:" OPTION
	do 
		case $OPTION in
			t)
			CRON_TIME=$OPTARG
			;;
			*)
			exit 1
			;;
		esac
	done 
fi

sudo chmod 666 /etc/environment

#Set source directory
echo -n "Enter source directory: "
read S_DIR
sudo sed -i "/SOURCE_DIR/d" /etc/environment
sudo echo "SOURCE_DIR=$S_DIR" >> /etc/environment

#Set destination directory
echo -n "Enter destination directory: "
read D_DIR
sudo sed -i "/DESTINATION_DIR/d" /etc/environment
sudo echo "DESTINATION_DIR=$D_DIR" >> /etc/environment
sudo chmod 644 /etc/environment

sudo chmod 666 $SCRIPT_SDIR/c_tab

sudo echo "$CRON_TIME $SCRIPT_SDIR/archive.sh" >> $SCRIPT_SDIR/c_tab

crontab $SCRIPT_SDIR/c_tab
